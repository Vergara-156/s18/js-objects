let bulbasaur = new Pokemon('Bulbasaur', 3, 10, 5);
let charizard = new Pokemon('Charizard', 2, 20, 11); 

function Pokemon(name, lvl, hp, atk){
	this.name = name;
	this.level = lvl;
	this.health = hp * 2;
	this.attack = atk;

	// methods
	this.tackle = function(target){
		console.log(this.name + ' '+ 'tackled ' + target.name)
	
	};

	
	this.hpstatus = function(target){
this.health = this.health - target.attack
		console.log(this.name + ' hp is: ' + this.health)
		
	}

	this.faint = function(){

		if(this.health<10){
		console.log(this.name + ' ' + 'fainted')
		}
		else{

			console.log(this.name +  ' ' +  'not yet fainted')
		}
	};
}

// creating instance



bulbasaur.tackle(charizard);
charizard.hpstatus(bulbasaur);

charizard.tackle(bulbasaur);
bulbasaur.hpstatus(charizard);

bulbasaur.tackle(charizard);
charizard.hpstatus(bulbasaur);


bulbasaur.faint(bulbasaur);
charizard.faint(charizard);